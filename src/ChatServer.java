import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Main class of the chat server, Handle connection from clients
 * @author Jacques CHARNAY & Hugo REYMOND
 */
public class ChatServer {
    private static List<ClientThread> connectedClients;

    /**
     * List of all the message of the actual conversation
     */
    private static List<String> messages;
    
    /**
     * main method
     */
    public static void main(String args[]){
        ServerSocket listenSocket;

        if (args.length != 1) {
            System.out.println("Usage: java EchoServer <EchoServer port>");
            System.exit(1);
        }

        connectedClients = new ArrayList<>();
        messages = new ArrayList<>();

        initHistory();
        CommandThread commandThread = new CommandThread();
        commandThread.start();
        
        WebThread webThread = new WebThread();
        webThread.start();

        try {
            //We receive client connection and create dedicated threads
            listenSocket = new ServerSocket(Integer.parseInt(args[0])); //port
            System.out.println("Server ready...");
            while (true) {
                Socket clientSocket = listenSocket.accept();
                System.out.println("Connexion from:" + clientSocket.getInetAddress());
                ClientThread ct = new ClientThread(clientSocket);
                ct.start();
                connectedClients.add(ct);
            }
        } catch (Exception e) {
            System.err.println("Error:" + e);
        }
        saveHistory();
    }

    /**
     * At the server launch, we recover the previous conversation from "history.txt" file
     */
    public static void initHistory(){
        File savefile = new File("history.txt");
        if(savefile.exists()){
            try {
                BufferedReader fis = new BufferedReader(new FileReader(savefile));
                messages = fis.lines().collect(Collectors.toList());
                fis.close();
            } catch (FileNotFoundException e) {
                System.out.println("Impossible d'ouvrir le fichier history.txt en lecture");
            } catch (IOException e) {
                System.out.println("Impossible d'ouvrir le fichier history.txt en lecture");
            }
        }
        else{
            System.out.println("(Pas encore d'historique ?)");
        }
    }

    /**
     * Save the current conversation into file "history.txt"
     */
    public static void saveHistory(){
        File savefile = new File("history.txt");
        try {
            BufferedWriter fis = new BufferedWriter(new FileWriter(savefile));
            for(String msg: messages){
                fis.write(msg);
                fis.newLine();
            }
            fis.close();
        } catch (FileNotFoundException e) {
            System.out.println("Impossible d'ouvrir le fichier history.txt en lecture");
        } catch(IOException e){
            System.out.println("Impossible d'ouvrir le fichier history.txt en lecture");
        }
    }

    /**
     * Send a message to all connected clients
     * @param message
     */
    public static void publishMessage(String message){
        System.out.println(message);
        messages.add(message);
        for(ClientThread ct: connectedClients){
            ct.sendMessage(message);
        }
        saveHistory();
    }

    /**
     * Send all the conversation to a client
     * (this is used when the client join the conversation)
     * @param clientThread
     */
    public static void sendHistory(ClientThread clientThread) {
        for(String message: messages){
            clientThread.sendMessage(message);
        }
    }

    /**
     * delete the current conversation and associated file
     */
    public static void deleteHistory() {
        messages.clear();
        saveHistory();
    }

    /**
     * @return the list of connected clients
     */
    public static List<String> getConnectedClientsPseudos(){
        List<String> clients = new ArrayList();
        for(ClientThread c: connectedClients){
            clients.add(c.getPseudo());
        }
        return clients;
    }

    /**
     * remove a client thread from the list of connected clients
     * @param clientThread
     */
    public static void removeClient(ClientThread clientThread){
        connectedClients.remove(clientThread);
    }

}



