import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Thread to handle server admin command from command line
 * @author Hugo REYMOND
 */
public class CommandThread extends Thread {

    /**
     * Wait for admin commands
     */
    public void run(){
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));

        while(true){
            try {
                String cmd = stdIn.readLine();
                if(cmd.equalsIgnoreCase("quit")){
                    System.out.println("Closing server...");
                    ChatServer.saveHistory();
                    System.exit(0);
                }else if(cmd.equalsIgnoreCase("save")){
                    System.out.println("Saving history...");
                    ChatServer.saveHistory();
                }else if(cmd.equalsIgnoreCase("delete")){
                    System.out.println("Deleting history...");
                    ChatServer.deleteHistory();
                }else{
                    System.out.println("Commande non reconnue :/");
                    System.out.println("Commandes disponibles: ");
                    System.out.println("quit - Fermer le serveur");
                    System.out.println("save - Sauvegarder l'historique");
                    System.out.println("delete - Supprimer l'historique");
                }

            } catch (IOException e) {
                System.err.println("Impossible de lire la console");
            }
        }

    }

}
