
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Web interface to see the numbers of connected users
 * @author Hugo REYMOND
 */
public class WebThread extends Thread {
    
    public void run() {
        try {
            ServerSocket listeningSocket = new ServerSocket(3002);
            while(true) {

                Socket client = listeningSocket.accept();
                BufferedReader in = new BufferedReader(new InputStreamReader(
                    client.getInputStream()));
                PrintWriter out = new PrintWriter(client.getOutputStream());
                 
                String str = ".";
                while(!str.equals(""))
                    str = in.readLine();
                //System.out.println("Test1");
                out.println("HTTP/1.0 200 OK");
                out.println("Content-Type: text/html");
                out.println("");
                
                out.println("<h1 align=center>ChatIF</h1>");
                List<String> clients = ChatServer.getConnectedClientsPseudos();
                out.println("<p>Il y a actuellement " + clients.size() + " personnes connectees</p>");
                out.println("<p>Leur pseudos sont les suivants </p> <ul>");
                for(String c:clients){
                    out.println("<li>"+c +"</li>");
                }
                out.println("</ul>");
                out.flush();
                client.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(WebThread.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
