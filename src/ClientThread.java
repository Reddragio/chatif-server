import java.io.*;
import java.net.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Thread to handle connection with one chat client
 * @author Jacques CHARNAY & Hugo REYMOND
 */
public class ClientThread extends Thread {
    private static DateFormat dateFormat = new SimpleDateFormat("HH:mm");

    private Socket clientSocket;
    private String pseudo = "Inconnu";
    PrintStream socOut = null;

    public String getPseudo() {
        return pseudo;
    }
    
    ClientThread(Socket s) {
        this.clientSocket = s;
    }

    /**
     * Send a message to the client
     * @param message
     */
    public void sendMessage(String message){
        socOut.println(message);
    }

    /**
     * receive and send messages from client
     */
    public void run() {
        try {
            String message;
            BufferedReader socIn = null;
            socIn = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));
            socOut = new PrintStream(clientSocket.getOutputStream());

            while (true) {
                String line = "";
                line = socIn.readLine();
                while(socIn.ready()){
                    line += "<br/>"+socIn.readLine();
                }
                if(line.length() > 0){
                    switch (line.charAt(0)){
                        case 'j':
                            //Join special message
                            pseudo = line.substring(1);
                            ChatServer.sendHistory(this);
                            ChatServer.publishMessage("<i>Nouvel arrivant sur le chat: "+pseudo+"</i>");
                            break;

                        case 'm':
                            //normal message, to add into conversation
                            message = line.substring(1);
                            ChatServer.publishMessage(dateFormat.format(new Date()) + " - <b>"+pseudo + "</b>: " + message);
                            break;

                        default:
                            System.out.println("Commande inconnue...");
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("Error in EchoServer:" + e);
        }
        ChatServer.publishMessage("<i>"+pseudo+" a quitté le chat"+"</i>");
        System.out.println("Fin de la communication");

        ChatServer.removeClient(this);
    }

}


